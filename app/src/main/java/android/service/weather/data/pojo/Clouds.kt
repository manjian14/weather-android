package android.service.weather.data.pojo

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


@Entity
data class Clouds(
    @SerializedName("all")
    @Expose
    @ColumnInfo(name = "all")
    var all: Int? = null
)


