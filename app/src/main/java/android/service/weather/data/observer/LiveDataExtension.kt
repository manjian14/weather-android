package android.service.weather.data.observer

import androidx.annotation.MainThread
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.*

class NonNullMediatorLiveData<T> : MediatorLiveData<T>()

typealias ObserveTrigger<T> = ((T) -> Unit)

@MainThread
fun <T> LiveData<T>.nonNull(): NonNullMediatorLiveData<T> {
    val mediator: NonNullMediatorLiveData<T> =
        NonNullMediatorLiveData()
    mediator.addSource(this) { it ->
        it?.let { mediator.value = it }
    }
    return mediator
}

@MainThread
inline fun <T> NonNullMediatorLiveData<T>.withObserve(owner: LifecycleOwner, crossinline onChanged: ObserveTrigger<T>) {
    this.removeObservers(owner)
    this.observe(owner, Observer { it ->
        it?.let { onChanged.invoke(it) }
    })

}

@MainThread
fun <T> LiveData<T>.observeOnce(owner: LifecycleOwner, observer: Observer<T>) {
    observe(owner, object : Observer<T> {
        override fun onChanged(t: T?) {
            t?.let {
                observer.onChanged(t)
                removeObserver(this)
            }

        }
    })
}

@MainThread
inline fun <reified T : ViewModel> FragmentActivity.getViewModel(): T {
    return ViewModelProviders.of(this)[T::class.java]
}

@MainThread
inline fun <reified T : ViewModel> FragmentActivity.withViewModel(crossinline body: T.() -> Unit): T {
    val vm:T = getViewModel()
    vm.body()
    return vm
}



