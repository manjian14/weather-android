package android.service.weather.main

import android.os.Bundle
import android.service.weather.R
import android.service.weather.data.pojo.WeatherData
import android.service.weather.databinding.FragmentWeatherBinding
import android.service.weather.utility.Utility
import android.service.weather.utility.showToast
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import org.koin.android.viewmodel.ext.android.viewModel


class WeatherFragment : Fragment() {

    private val weatherViewModel: WeatherViewModel by viewModel()
    private lateinit var binding: FragmentWeatherBinding

    private val ioObserver = Observer<WeatherData> { weatherData ->
        weatherViewModel.insertNetworkData(weatherData)
    }

    private val cacheObserver = Observer<WeatherData> { weatherData ->
        if (weatherData == null && !Utility.isNetworkAvailable(requireActivity())) {
            val text: CharSequence = resources.getText(R.string.disabledNetwork)
            weatherViewModel.liveData.value = text.toString()
        } else {
            weatherData?.run {
                weatherViewModel.liveData.value = weatherData.toString()
            }

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_weather, container, false)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(this, onBackPressedCallback);
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            viewModel = weatherViewModel
            lifecycleOwner = this@WeatherFragment
            listener = View.OnClickListener { fetchData() }
        }

        fetchData()
    }

    private fun fetchData() {
        weatherViewModel.weatherCache.observe(requireActivity(), cacheObserver)
        if (Utility.isNetworkAvailable(requireContext())) {
            requireActivity().showToast("Calling API")
            weatherViewModel.weatherIO.observe(requireActivity(), ioObserver)
        } else {
            requireActivity().showToast(resources.getString(R.string.disabledNetwork))
        }

    }

    private val onBackPressedCallback: OnBackPressedCallback =
        object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                requireActivity().finishAffinity()
            }
        }


}