package android.service.weather.data.pojo

import androidx.room.ColumnInfo
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


data class Coord(
    @SerializedName("lon")
    @Expose
    @ColumnInfo(name = "lon")
    var lon: Double? = null,

    @SerializedName("lat")
    @Expose
    @ColumnInfo(name = "lat")
    var lat: Double? = null
)


