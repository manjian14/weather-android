package android.service.weather.main

import android.service.weather.data.database.weatherTable.WeatherTableRepository
import android.service.weather.data.pojo.WeatherData
import android.service.weather.io.Status
import android.service.weather.io.WeatherRepository
import android.service.weather.utility.default
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.room.Transaction
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking


class WeatherViewModel(
    private val weatherRepo: WeatherRepository,
    private val weatherTableRepository: WeatherTableRepository
) : ViewModel() {


    val liveData: MutableLiveData<String> = MutableLiveData<String>().default("Initial value!")

    val weatherIO: LiveData<WeatherData> = liveData(Dispatchers.IO) {
        weatherRepo.getWeather()
            .map {
                when (it.status) {
                    Status.SUCCESS -> it.data
                    Status.ERROR -> null
                }
            }.collect {
                it?.run {
                    emit(this)
                }

            }

    }

    val weatherCache: LiveData<WeatherData> = weatherTableRepository.getWeatherDataFromTable()

    @Transaction
    fun insertNetworkData(weatherData: WeatherData) = runBlocking(Dispatchers.IO) {
        weatherTableRepository.deleteCacheData()
        weatherTableRepository.insertWeatherData(weatherData)
    }


}