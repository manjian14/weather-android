package android.service.weather.data.database

import android.content.Context
import android.service.weather.data.database.weatherTable.WeatherDao
import android.service.weather.data.pojo.WeatherData
import android.service.weather.utility.Constant
import android.service.weather.utility.SingletonHolder
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [WeatherData::class], version = 1)
@TypeConverters(DataTypeConverter::class)
abstract class AppDataBase : RoomDatabase() {

    companion object : SingletonHolder<AppDataBase, Context>({ context ->
        Room.databaseBuilder(
            context.applicationContext,
            AppDataBase::class.java, Constant.DATABASE_NAME
        ).fallbackToDestructiveMigration().build()
    })

    abstract fun weatherTableDao(): WeatherDao

}