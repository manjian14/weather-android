package android.service.weather.data.database.weatherTable

import android.service.weather.data.pojo.WeatherData
import androidx.lifecycle.LiveData

class WeatherTableRepository constructor(private val weatherDao: WeatherDao) {


    suspend fun insertWeatherData(weatherData: WeatherData) {
        weatherDao.insert(weatherData)
    }

    suspend fun deleteCacheData(){
        weatherDao.delete()
    }

    fun getWeatherDataFromTable(): LiveData<WeatherData> {
        return weatherDao.getData()
    }
}