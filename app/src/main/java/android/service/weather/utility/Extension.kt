package android.service.weather.utility

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.lifecycle.MutableLiveData

fun Context.showToast(text: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, text, duration).show().apply {  }
}

fun <T : Any?> MutableLiveData<T>.default(initialValue: T):MutableLiveData<T> = apply { setValue(initialValue) }

inline fun <reified T : View> Activity.bind(@IdRes res: Int): Lazy<T> =
    lazy(LazyThreadSafetyMode.NONE) { findViewById<T>(res) }

fun ViewGroup.inflate(@LayoutRes layoutRes: Int): View =
    LayoutInflater.from(context).inflate(layoutRes, this, false)