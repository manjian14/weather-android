package android.service.weather.data.database.weatherTable

import android.service.weather.data.pojo.WeatherData
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface WeatherDao : BaseDao<WeatherData> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    override suspend fun insert(data: WeatherData)

    @Query("SELECT * FROM WeatherData")
    override fun getData(): LiveData<WeatherData>

    @Query("DELETE FROM WeatherData")
    override suspend fun delete()
}