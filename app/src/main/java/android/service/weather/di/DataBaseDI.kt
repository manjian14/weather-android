package android.service.weather.di

import android.content.Context
import android.service.weather.data.database.AppDataBase
import android.service.weather.data.database.weatherTable.WeatherDao
import android.service.weather.data.database.weatherTable.WeatherTableRepository
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val dataBaseModule = module {
    single { provideDataBase(androidContext()) }
    factory { provideWeatherTableRepository(get()) }
    factory { provideWeatherDao(get()) }

}

fun provideDataBase(context: Context): AppDataBase = AppDataBase.getInstance(context)

fun provideWeatherDao(appDataBase: AppDataBase): WeatherDao = appDataBase.weatherTableDao()

fun provideWeatherTableRepository(weatherDao: WeatherDao):WeatherTableRepository = WeatherTableRepository(weatherDao)


