package android.service.weather.utility

open class SingletonHolder<out T : Any, in A>(creator: (A) -> T) {
    private var creator: ((A) -> T)? = creator
    @Volatile
    private var instance: T? = null

    fun getInstance(arg: A): T {
        return instance ?: synchronized(this) {
            val i2: T? = instance
            if (i2 != null) {
                i2
            } else {
                val created: T = creator!!(arg)
                instance = created
                creator = null
                created
            }
        }
    }
}