package android.service.weather.data.pojo

import android.service.weather.data.database.DataTypeConverter
import androidx.room.*
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


@Entity(tableName = "WeatherData")
data class WeatherData(

    @PrimaryKey
    @SerializedName("dt")
    @Expose
    @ColumnInfo(name = "dt")
    val dt: Int? = null,

    @SerializedName("coord")
    @Expose
    @Embedded(prefix = "coord")
    val coord: Coord? = null,

    @SerializedName("weather")
    @Expose
    @ColumnInfo(name = "weather")
    @TypeConverters(DataTypeConverter::class)
    val weather: List<Weather>? = null,

    @SerializedName("base")
    @Expose
    @ColumnInfo(name = "base")
    val base: String? = null,

    @SerializedName("main")
    @Expose
    @Embedded(prefix = "main")
    val main: Main? = null,

    @SerializedName("visibility")
    @Expose
    @ColumnInfo(name = "visibility")
    val visibility: Int? = null,

    @SerializedName("wind")
    @Expose
    @Embedded(prefix = "wind")
    val wind: Wind? = null,

    @SerializedName("clouds")
    @Expose
    @Embedded(prefix = "clouds")
    val clouds: Clouds? = null,

    @SerializedName("sys")
    @Expose
    @Embedded(prefix = "sys")
    val sys: Sys? = null,

    @SerializedName("timezone")
    @Expose
    @ColumnInfo(name = "timezone")
    val timezone: Int? = null,

    @SerializedName("id")
    @Expose
    @ColumnInfo(name = "id")
    val id: Int? = null,

    @SerializedName("name")
    @Expose
    @ColumnInfo(name = "name")
    val name: String? = null,

    @SerializedName("cod")
    @Expose
    @ColumnInfo(name = "cod")
    val cod: Int? = null
)

