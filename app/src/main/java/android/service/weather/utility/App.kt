package android.service.weather.utility

import android.service.weather.di.dataBaseModule
import android.service.weather.di.networkModule
import android.service.weather.di.viewModelModule
import android.service.weather.di.weatherModule
import androidx.multidex.MultiDexApplication
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(listOf(networkModule,viewModelModule, weatherModule, dataBaseModule))
        }

    }
}