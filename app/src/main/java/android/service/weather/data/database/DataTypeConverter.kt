package android.service.weather.data.database

import android.service.weather.data.pojo.Weather
import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*


class DataTypeConverter {
    private val gson = Gson()

    @TypeConverter
    fun stringToList(data: String?): List<Weather> {
        if (data == null) {
            return Collections.emptyList()
        }

        return gson.fromJson(
            data,
            object : TypeToken<List<Weather?>?>() {}.type
        )
    }

    @TypeConverter
    fun listToString(someObjects: List<Weather?>?): String {
        return gson.toJson(someObjects)
    }
}