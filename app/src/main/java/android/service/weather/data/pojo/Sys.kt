package android.service.weather.data.pojo

import androidx.room.ColumnInfo
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


data class Sys(
    @SerializedName("type")
    @Expose
    @ColumnInfo(name = "type")
    var type: Int? = null,

    @SerializedName("id")
    @Expose
    @ColumnInfo(name = "id")
    var id: Int? = null,

    @SerializedName("country")
    @Expose
    @ColumnInfo(name = "country")
    var country: String? = null,

    @SerializedName("sunrise")
    @Expose
    @ColumnInfo(name = "sunrise")
    var sunrise: Int? = null,

    @SerializedName("sunset")
    @Expose
    @ColumnInfo(name = "sunset")
    var sunset: Int? = null
)