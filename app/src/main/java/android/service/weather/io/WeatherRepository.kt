package android.service.weather.io

import android.service.weather.data.pojo.WeatherData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow


class WeatherRepository(private val weatherApi: WeatherApi) {

    suspend fun getWeather(): Flow<Resource<WeatherData>> = flow {
        try {
            emit(ResponseHandler<WeatherData>().handleSuccess(weatherApi.getWeatherData()))
        } catch (e: Exception) {
            emit(ResponseHandler<WeatherData>().handleException(e))

        }
    }
}