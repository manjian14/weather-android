package android.service.weather.io

import android.service.weather.data.pojo.WeatherData
import android.service.weather.utility.Constant
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {
    @GET("data/2.5/weather")
    suspend fun getWeatherData(
        @Query("q") city: String = "yerevan",
        @Query("appid") apiKey: String = Constant.API_KEY
    ): WeatherData
}