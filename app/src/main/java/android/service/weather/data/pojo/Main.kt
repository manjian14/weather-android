package android.service.weather.data.pojo

import androidx.room.ColumnInfo
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


data class Main(
    @SerializedName("temp")
    @Expose
    @ColumnInfo(name = "temp")
    var temp: Double? = null,

    @SerializedName("feels_like")
    @Expose
    @ColumnInfo(name = "feels_like")
    var feelsLike: Double? = null,

    @SerializedName("temp_min")
    @Expose
    @ColumnInfo(name = "temp_min")
    var tempMin: Double? = null,

    @SerializedName("temp_max")
    @Expose
    @ColumnInfo(name = "temp_max")
    var tempMax: Double? = null,

    @SerializedName("pressure")
    @Expose
    @ColumnInfo(name = "pressure")
    var pressure: Int? = null,

    @SerializedName("humidity")
    @Expose
    @ColumnInfo(name = "humidity")
    var humidity: Int? = null
)


