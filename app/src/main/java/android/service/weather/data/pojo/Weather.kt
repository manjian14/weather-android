package android.service.weather.data.pojo

import androidx.room.ColumnInfo
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


data class Weather(
    @SerializedName("id")
    @Expose
    @ColumnInfo(name = "id")
    var id: Int? = null,

    @SerializedName("main")
    @Expose
    @ColumnInfo(name = "main")
    var main: String? = null,

    @SerializedName("description")
    @Expose
    @ColumnInfo(name = "description")
    var description: String? = null,

    @SerializedName("icon")
    @Expose
    @ColumnInfo(name = "icon")
    var icon: String? = null
)


