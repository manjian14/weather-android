package android.service.weather.utility

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities

object Utility {

    fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager: ConnectivityManager? =  context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val network: Network? = connectivityManager?.activeNetwork
            val capabilities: NetworkCapabilities? = connectivityManager?.getNetworkCapabilities(network)
            return  capabilities != null && (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) || capabilities.hasTransport(
                NetworkCapabilities.TRANSPORT_CELLULAR
            ))


    }
}