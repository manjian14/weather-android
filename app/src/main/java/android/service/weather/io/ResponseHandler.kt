package android.service.weather.io

import retrofit2.HttpException

open class ResponseHandler<T> {
    fun handleSuccess(data: T): Resource<T> {
        return Resource.success(data)
    }

    fun  handleException(e: Exception): Resource<T> {
        return when (e) {
            is HttpException -> Resource.error(getErrorMessage(e.code()), null)
            else -> Resource.error(getErrorMessage(Int.MAX_VALUE), null)
        }
    }

    private fun getErrorMessage(code: Int): String {
        return when (code) {
            401 -> "Unauthorised"
            404 -> "Not found"
            else -> "Something went wrong"
        }
    }
}