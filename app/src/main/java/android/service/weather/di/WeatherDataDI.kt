package android.service.weather.di

import android.service.weather.data.database.weatherTable.WeatherTableRepository
import android.service.weather.io.WeatherApi
import android.service.weather.io.WeatherRepository
import android.service.weather.main.WeatherViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { provideWeatherViewModel(weatherRepo = get(), weatherTableRepository = get()) }
}

val weatherModule = module {
    factory { provideWeatherRepository(weatherApi = get()) }
}


fun provideWeatherViewModel(
    weatherRepo: WeatherRepository,
    weatherTableRepository: WeatherTableRepository
): WeatherViewModel = WeatherViewModel(weatherRepo, weatherTableRepository)


fun provideWeatherRepository(weatherApi: WeatherApi): WeatherRepository =
    WeatherRepository(weatherApi)