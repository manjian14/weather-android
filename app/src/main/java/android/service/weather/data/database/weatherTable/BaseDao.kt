package android.service.weather.data.database.weatherTable

import androidx.lifecycle.LiveData

interface BaseDao<T> {

    suspend fun insert(data: T)

    fun getData(): LiveData<T>

    suspend fun delete()
}