package android.service.weather.data.observer

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel

class LiveViewModel<T> : ViewModel() {

    private val liveData = LiveEvent<T>()

    val state: LiveData<T> = liveData

    fun update(value :T) {
        liveData.value = value
    }

}