package android.service.weather.data.pojo

import androidx.room.ColumnInfo
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


data class Wind(
    @SerializedName("speed")
    @Expose
    @ColumnInfo(name = "speed")
    var speed: Double? = null,

    @SerializedName("deg")
    @Expose
    @ColumnInfo(name = "deg")
    var deg: Int? = null
)