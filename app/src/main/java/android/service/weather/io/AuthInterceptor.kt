package android.service.weather.io

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class AuthInterceptor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val req:Request = chain.request()
        return chain.proceed(req)
    }
}